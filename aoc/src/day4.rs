use crate::intcode;
use crate::Day;
use input::Input;
use snafu::{ResultExt, Snafu};

#[derive(Debug, Snafu)]
pub enum Error {
    RetrievingInput { source: input::Error },
    ParseInput { source: std::num::ParseIntError },
}

pub struct Day4<'a> {
    cache: &'a Input,
}

impl<'a> Day4<'a> {
    fn is_valid(&self, val: i64) -> bool {
        let mut digits = intcode::split_digits(val);
        digits.reverse();
        let mut double = false;
        let mut previous = None;
        for digit in digits {
            if let Some(previous) = previous {
                if previous > digit {
                    return false;
                }
                if previous == digit {
                    double = true;
                }
            }
            previous = Some(digit);
        }
        double
    }

    fn part2_valid(&self, val: i64) -> bool {
        let mut digits = intcode::split_digits(val);
        digits.reverse();
        let mut double = false;
        let mut duplicates = 0;
        let mut previous = None;
        for digit in digits {
            if let Some(previous) = previous {
                if previous > digit {
                    return false;
                }
                if previous == digit {
                    duplicates += 1;
                } else {
                    if duplicates == 1 {
                        double = true;
                    } else {
                        duplicates = 0;
                    }
                }
            }
            previous = Some(digit);
        }
        if duplicates == 1 {
            double = true;
        }
        double
    }

    fn do_run(&self) -> Result<String, Box<dyn std::error::Error>> {
        let range = self
            .input()
            .context(RetrievingInput)?
            .trim()
            .split('-')
            .map(|s| s.parse::<i64>())
            .collect::<Result<Vec<_>, _>>()
            .context(ParseInput)?;
        let mut part1 = 0;
        let mut part2 = 0;

        for i in range[0]..range[1] {
            if self.is_valid(i) {
                part1 += 1;
            }
            if self.part2_valid(i) {
                part2 += 1;
            }
        }
        Ok(format!("Part1 {} Part2 {}", part1, part2))
    }
}

impl<'a> Day for Day4<'a> {
    fn day(&self) -> u8 {
        4
    }

    fn cache(&self) -> &Input {
        self.cache
    }

    fn run(&self) -> Result<String, Box<dyn std::error::Error>> {
        self.do_run()
    }
}

pub fn new<'a>(cache: &'a Input) -> impl Day + 'a {
    Day4 { cache }
}
