use crate::intcode::{self, Computer};
use crate::Day;
use input::Input;
use snafu::{ResultExt, Snafu};

#[derive(Debug, Snafu)]
pub enum Error {
    RetrievingInput { source: input::Error },
    ParseInput { source: std::num::ParseIntError },
    IntCode { source: intcode::Error },
}

pub struct Day2<'a> {
    cache: &'a Input,
}

impl<'a> Day2<'a> {
    fn do_run(&self) -> Result<String, Box<dyn std::error::Error>> {
        let opcodes = self
            .input()
            .context(RetrievingInput)?
            .trim()
            .split(',')
            .map(|s| s.parse::<i64>())
            .collect::<Result<Vec<_>, _>>()
            .context(ParseInput)?;
        let mut computer = Computer::new(&opcodes);
        computer.set(1, 12);
        computer.set(2, 2);
        computer.run(None, None)?;
        let part1 = computer.index(0);

        'outer: for noun in 0..99 {
            for verb in 0..99 {
                computer.reset();
                computer.set(1, noun);
                computer.set(2, verb);
                computer.run(None, None)?;
                if computer.index(0) == 19690720 {
                    break 'outer;
                }
            }
        }
        let part2 = computer.index(1) * 100 + computer.index(2);
        Ok(format!("Part 1 {} Part2 {}", part1, part2))
    }
}

impl<'a> Day for Day2<'a> {
    fn day(&self) -> u8 {
        2
    }

    fn cache(&self) -> &Input {
        self.cache
    }

    fn run(&self) -> Result<String, Box<dyn std::error::Error>> {
        self.do_run()
    }
}

pub fn new<'a>(cache: &'a Input) -> impl Day + 'a {
    Day2 { cache }
}
