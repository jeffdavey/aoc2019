use crate::intcode::{self, Computer, IntCodeIO};
use crate::Day;
use input::Input;
use snafu::{ResultExt, Snafu};

#[derive(Debug, Snafu)]
pub enum Error {
    RetrievingInput { source: input::Error },
    ParseInput { source: std::num::ParseIntError },
    IntCode { source: intcode::Error },
}

pub struct Day5<'a> {
    cache: &'a Input,
}

impl<'a> Day5<'a> {
    fn do_run(&self) -> Result<String, Box<dyn std::error::Error>> {
        let opcodes = self
            .input()
            .context(RetrievingInput)?
            .trim()
            .split(',')
            .map(|s| s.parse::<i64>())
            .collect::<Result<Vec<_>, _>>()
            .context(ParseInput)?;

        let input = IntCodeIO::from_values(vec![1]);
        let output = IntCodeIO::new();
        let mut computer = Computer::new(&opcodes);
        computer.run(Some(input), Some(output.clone()))?;
        let part1 = output.values();

        let input = IntCodeIO::from_values(vec![5]);
        let output = IntCodeIO::new();
        computer.reset();
        computer.run(Some(input), Some(output.clone()))?;
        let part2 = output.values();

        Ok(format!("Part 1: {:?} Part 2: {:?}", part1, part2))
    }
}

impl<'a> Day for Day5<'a> {
    fn day(&self) -> u8 {
        5
    }

    fn cache(&self) -> &Input {
        self.cache
    }

    fn run(&self) -> Result<String, Box<dyn std::error::Error>> {
        self.do_run()
    }
}

pub fn new<'a>(cache: &'a Input) -> impl Day + 'a {
    Day5 { cache }
}
