use crate::Day;
use input::Input;
use snafu::{ResultExt, Snafu};

#[derive(Debug, Snafu)]
pub enum Error {
    RetrievingInput { source: input::Error },
    ParsingInput { source: std::num::ParseIntError },
}

pub struct Day1<'a> {
    cache: &'a Input,
}

impl<'a> Day for Day1<'a> {
    fn day(&self) -> u8 {
        1
    }

    fn cache(&self) -> &Input {
        self.cache
    }

    fn run(&self) -> Result<String, Box<dyn std::error::Error>> {
        let masses = self
            .input()
            .context(RetrievingInput)?
            .lines()
            .map(|l| l.parse::<i32>())
            .collect::<Result<Vec<_>, _>>()
            .context(ParsingInput)?;
        let part1 = masses.iter().fold(0, |val, mass| val + (mass / 3) - 2);
        let part2 = masses.iter().fold(0, |val, mass| {
            let fuel = (mass / 3) - 2;
            let mut fuel_weight = 0;
            let mut cur = fuel;
            loop {
                cur = (cur / 3) - 2;
                if cur > 0 {
                    fuel_weight += cur;
                } else {
                    break;
                }
            }
            val + fuel + fuel_weight
        });

        Ok(format!("Part 1 {} Part 2 {}", part1, part2))
    }
}

pub fn new<'a>(cache: &'a Input) -> impl Day + 'a {
    Day1 { cache }
}
