use crate::Day;
use input::Input;
use snafu::{ResultExt, Snafu};

#[derive(Debug, Snafu)]
pub enum Error {
    RetrievingInput { source: input::Error },
    ParsingInput { source: std::num::ParseIntError },
}

pub struct Day8<'a> {
    cache: &'a Input,
}

impl<'a> Day for Day8<'a> {
    fn day(&self) -> u8 {
        8
    }

    fn cache(&self) -> &Input {
        self.cache
    }

    fn run(&self) -> Result<String, Box<dyn std::error::Error>> {
        let pixels = self
            .input()
            .context(RetrievingInput)?
            .trim()
            .chars()
            .map(|c| c.to_digit(10).expect("Bad digit"))
            .collect::<Vec<_>>();

        let width = 25;
        let height = 6;
        let mut layers = Vec::new();

        let mut remaining = pixels.as_slice();
        loop {
            let (current, new) = remaining.split_at(width * height);
            layers.push(current);
            if new.len() == 0 {
                break;
            }
            remaining = new;
        }

        let mut least_zeroes = 0;
        let mut part1 = 0;
        for layer in &layers {
            let mut zeroes = 0;
            let mut ones = 0;
            let mut twos = 0;
            for pixel in *layer {
                match pixel {
                    0 => zeroes += 1,
                    1 => ones += 1,
                    2 => twos += 1,
                    _ => {}
                }
            }
            if least_zeroes == 0 || zeroes < least_zeroes {
                least_zeroes = zeroes;
                part1 = ones * twos;
            }
        }
        layers.reverse();

        let mut message = vec![2u32; height * width];

        for layer in layers {
            for (index, pixel) in layer.iter().enumerate() {
                match pixel {
                    0 | 1 => message[index] = *pixel,
                    _ => {}
                }
            }
        }

        let mut part2 = String::new();
        for y in 0..height {
            for x in 0..width {
                match message[(y * width) + x] {
                    0 => part2.push(' '),
                    1 => part2.push('X'),
                    2 => part2.push(' '),
                    _ => {}
                }
            }
            part2.push('\n')
        }

        Ok(format!("Part 1: {}\n{}", part1, part2))
    }
}

pub fn new<'a>(cache: &'a Input) -> impl Day + 'a {
    Day8 { cache }
}
