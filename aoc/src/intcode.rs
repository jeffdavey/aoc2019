use num_enum::TryFromPrimitive;
use snafu::{OptionExt, Snafu};
use std::collections::VecDeque;
use std::convert::TryFrom;
use std::sync::{Arc, Condvar, Mutex};

type IntCodeIOWrapper = Arc<(Mutex<VecDeque<i64>>, Condvar)>;
#[derive(Debug, Clone)]
pub struct IntCodeIO(IntCodeIOWrapper);

#[derive(Debug, Snafu)]
pub enum Error {
    OpCodeConversion {
        source: Box<dyn std::error::Error + Send>,
    },
    #[snafu(display("Unknown mode {}", mode))]
    UnknownMode {
        mode: i64,
    },
    NoInput,
    NoOutput,
}

#[derive(Debug, Eq, PartialEq, TryFromPrimitive)]
#[repr(i64)]
enum OpCode {
    Add = 1,
    Mul = 2,
    Input = 3,
    Output = 4,
    JumpTrue = 5,
    JumpFalse = 6,
    LessThan = 7,
    Equals = 8,
    AdjustBase = 9,
    End = 99,
}

enum InputMode {
    Position(usize),
    Immediate(i64),
    Relative(i64),
}

impl TryFrom<(i64, Option<i64>)> for InputMode {
    type Error = Error;

    fn try_from(val: (i64, Option<i64>)) -> Result<Self, Self::Error> {
        if let Some(mode) = val.1 {
            match mode {
                0 => Ok(InputMode::Position(val.0 as usize)),
                1 => Ok(InputMode::Immediate(val.0)),
                2 => Ok(InputMode::Relative(val.0)),
                _ => Err(Error::UnknownMode { mode }),
            }
        } else {
            Ok(InputMode::Position(val.0 as usize))
        }
    }
}

enum OutputMode {
    Position(usize),
    Relative(i64),
}

impl TryFrom<(i64, Option<i64>)> for OutputMode {
    type Error = Error;

    fn try_from(val: (i64, Option<i64>)) -> Result<Self, Self::Error> {
        if let Some(mode) = val.1 {
            match mode {
                0 => Ok(OutputMode::Position(val.0 as usize)),
                2 => Ok(OutputMode::Relative(val.0)),
                _ => Err(Error::UnknownMode { mode }),
            }
        } else {
            Ok(OutputMode::Position(val.0 as usize))
        }
    }
}

enum Operation {
    Add {
        input1: InputMode,
        input2: InputMode,
        output: OutputMode,
        size: usize,
    },
    Mul {
        input1: InputMode,
        input2: InputMode,
        output: OutputMode,
        size: usize,
    },
    Input {
        output: OutputMode,
        size: usize,
    },
    Output {
        input: InputMode,
        size: usize,
    },
    JumpTrue {
        input1: InputMode,
        input2: InputMode,
        size: usize,
    },
    JumpFalse {
        input1: InputMode,
        input2: InputMode,
        size: usize,
    },
    LessThan {
        input1: InputMode,
        input2: InputMode,
        output: OutputMode,
        size: usize,
    },
    Equals {
        input1: InputMode,
        input2: InputMode,
        output: OutputMode,
        size: usize,
    },
    AdjustBase {
        input: InputMode,
        size: usize,
    },
    End,
}

impl Operation {
    fn parse(index: usize, opcodes: &Vec<i64>) -> Result<Self, Error> {
        let digits = split_digits(opcodes[index]);
        let mut code = digits[0];
        if digits.len() > 1 {
            code += digits[1] * 10;
        }
        let param1_mode = if digits.len() > 2 { Some(digits[2]) } else { None };
        let param2_mode = if digits.len() > 3 { Some(digits[3]) } else { None };
        let param3_mode = if digits.len() > 4 { Some(digits[4]) } else { None };

        let opcode = OpCode::try_from(code).map_err(|e| Error::OpCodeConversion { source: Box::new(e) })?;
        Ok(match opcode {
            OpCode::Add => Operation::Add {
                input1: InputMode::try_from((opcodes[index + 1], param1_mode))?,
                input2: InputMode::try_from((opcodes[index + 2], param2_mode))?,
                output: OutputMode::try_from((opcodes[index + 3], param3_mode))?,
                size: 4,
            },
            OpCode::Mul => Operation::Mul {
                input1: InputMode::try_from((opcodes[index + 1], param1_mode))?,
                input2: InputMode::try_from((opcodes[index + 2], param2_mode))?,
                output: OutputMode::try_from((opcodes[index + 3], param3_mode))?,
                size: 4,
            },
            OpCode::Input => Operation::Input {
                output: OutputMode::try_from((opcodes[index + 1], param1_mode))?,
                size: 2,
            },
            OpCode::Output => Operation::Output {
                input: InputMode::try_from((opcodes[index + 1], param1_mode))?,
                size: 2,
            },
            OpCode::JumpTrue => Operation::JumpTrue {
                input1: InputMode::try_from((opcodes[index + 1], param1_mode))?,
                input2: InputMode::try_from((opcodes[index + 2], param2_mode))?,
                size: 3,
            },
            OpCode::JumpFalse => Operation::JumpFalse {
                input1: InputMode::try_from((opcodes[index + 1], param1_mode))?,
                input2: InputMode::try_from((opcodes[index + 2], param2_mode))?,
                size: 3,
            },
            OpCode::LessThan => Operation::LessThan {
                input1: InputMode::try_from((opcodes[index + 1], param1_mode))?,
                input2: InputMode::try_from((opcodes[index + 2], param2_mode))?,
                output: OutputMode::try_from((opcodes[index + 3], param3_mode))?,
                size: 4,
            },
            OpCode::Equals => Operation::Equals {
                input1: InputMode::try_from((opcodes[index + 1], param1_mode))?,
                input2: InputMode::try_from((opcodes[index + 2], param2_mode))?,
                output: OutputMode::try_from((opcodes[index + 3], param3_mode))?,
                size: 4,
            },
            OpCode::AdjustBase => Operation::AdjustBase {
                input: InputMode::try_from((opcodes[index + 1], param1_mode))?,
                size: 2,
            },
            OpCode::End => Operation::End,
        })
    }
}

pub fn split_digits(val: i64) -> Vec<i64> {
    let mut rtn = Vec::new();
    let mut current = 1;
    while current <= val {
        rtn.push((val / current) % 10);
        current *= 10;
    }
    rtn
}
impl IntCodeIO {
    pub fn new() -> Self {
        Self(Arc::new((Mutex::new(VecDeque::new()), Condvar::new())))
    }

    pub fn from_values(values: Vec<i64>) -> Self {
        Self(Arc::new((Mutex::new(VecDeque::from(values)), Condvar::new())))
    }

    pub fn values(&self) -> Vec<i64> {
        let (lock, _) = &*self.0;
        lock.lock().expect("Poisoned").iter().map(|i| *i).collect::<Vec<_>>()
    }

    pub fn push(&self, val: i64) {
        let (lock, _) = &*self.0;
        lock.lock().expect("Poisoned").push_back(val);
    }
}

pub struct Computer {
    memory: Vec<i64>,
    program: Vec<i64>,
    base: usize,
    index: usize,
}

impl Computer {
    pub fn new(opcodes: &Vec<i64>) -> Self {
        Self {
            base: 0,
            index: 0,
            memory: opcodes.to_vec(),
            program: opcodes.to_vec(),
        }
    }

    fn check_index(&mut self, index: usize) {
        if index >= self.memory.len() {
            self.memory.extend(std::iter::repeat(0).take((index - self.memory.len()) + 1));
        }
    }

    fn write(&mut self, output: OutputMode, value: i64) {
        let index = match output {
            OutputMode::Position(pos) => pos,
            OutputMode::Relative(pos) => (self.base as i64 + pos) as usize,
        };
        self.check_index(index);
        self.memory[index] = value;
    }

    pub fn set(&mut self, index: usize, value: i64) {
        self.memory[index] = value;
    }

    pub fn index(&mut self, index: usize) -> i64 {
        self.check_index(index);
        self.memory[index]
    }

    fn value(&mut self, input: InputMode) -> i64 {
        match input {
            InputMode::Position(pos) => self.index(pos),
            InputMode::Immediate(val) => val,
            InputMode::Relative(val) => self.index((self.base as i64 + val) as usize),
        }
    }

    pub fn reset(&mut self) {
        self.memory = self.program.to_vec();
        self.base = 0;
        self.index = 0;
    }

    pub fn run(&mut self, inputs: Option<IntCodeIO>, outputs: Option<IntCodeIO>) -> Result<(), Error> {
        loop {
            let operation = Operation::parse(self.index, &mut self.memory)?;
            match operation {
                Operation::Add { input1, input2, output, size } => {
                    let val = self.value(input1) + self.value(input2);
                    self.write(output, val);
                    self.index += size;
                }
                Operation::Mul { input1, input2, output, size } => {
                    let val = self.value(input1) * self.value(input2);
                    self.write(output, val);
                    self.index += size;
                }
                Operation::Input { output, size } => {
                    let (lock, cvar) = &*inputs.as_ref().context(NoInput)?.0;
                    let mut vec = lock.lock().expect("Mutex poisoned");
                    while vec.len() == 0 {
                        vec = cvar.wait(vec).expect("Poisoned");
                    }
                    self.write(output, vec.pop_front().unwrap());
                    self.index += size;
                }
                Operation::Output { input, size } => {
                    let (lock, cvar) = &*outputs.as_ref().context(NoOutput)?.0;
                    let mut vec = lock.lock().expect("Poisoned");
                    vec.push_back(self.value(input));
                    cvar.notify_one();
                    self.index += size;
                }
                Operation::JumpTrue { input1, input2, size } => {
                    if self.value(input1) != 0 {
                        self.index = self.value(input2) as usize;
                    } else {
                        self.index += size
                    }
                }
                Operation::JumpFalse { input1, input2, size } => {
                    if self.value(input1) == 0 {
                        self.index = self.value(input2) as usize;
                    } else {
                        self.index += size
                    }
                }
                Operation::LessThan { input1, input2, output, size } => {
                    if self.value(input1) < self.value(input2) {
                        self.write(output, 1);
                    } else {
                        self.write(output, 0);
                    }
                    self.index += size;
                }
                Operation::Equals { input1, input2, output, size } => {
                    if self.value(input1) == self.value(input2) {
                        self.write(output, 1);
                    } else {
                        self.write(output, 0);
                    }
                    self.index += size;
                }
                Operation::AdjustBase { input, size } => {
                    let new_base = self.base as i64 + self.value(input);
                    self.base = new_base as usize;
                    self.index += size;
                }
                Operation::End => {
                    break;
                }
            }
        }
        Ok(())
    }
}
