use crate::Day;
use input::Input;
use petgraph::graphmap::UnGraphMap;
use snafu::{OptionExt, ResultExt, Snafu};

#[derive(Debug, Snafu)]
pub enum Error {
    RetrievingInput { source: input::Error },
    ParseInput { source: std::num::ParseIntError },
    NegativeCycle,
    SantaNotFound,
}

pub struct Day6<'a> {
    cache: &'a Input,
}

impl<'a> Day6<'a> {
    fn do_run(&self) -> Result<String, Box<dyn std::error::Error>> {
        let orbits = self
            .input()
            .context(RetrievingInput)?
            .lines()
            .map(|l| l.trim().split(')').map(|s| s.to_string()).collect::<Vec<_>>())
            .collect::<Vec<_>>();

        let mut graph = UnGraphMap::new();
        for orbit in &orbits {
            // add both nodes
            if !graph.contains_node(&orbit[0]) {
                graph.add_node(&orbit[0]);
            }
            if !graph.contains_node(&orbit[1]) {
                graph.add_node(&orbit[1]);
            }
            graph.add_edge(&orbit[0], &orbit[1], 1.0);
        }
        let start = String::from("COM");
        let (paths, _) = petgraph::algo::bellman_ford(&graph, &start).map_err(|_| Error::NegativeCycle)?;
        let start = String::from("YOU");
        let end = String::from("SAN");
        let (cost, _path) = petgraph::algo::astar(&graph, &start, |target| target == &end, |e| *e.2, |_| 0.0).context(SantaNotFound)?;
        let part1 = paths.iter().fold(0.0, |cur, val| cur + *val);
        let part2 = cost - 2.0; // remove YOU & SAN
        Ok(format!("Part 1: {} Part 2: {}", part1, part2))
    }
}

impl<'a> Day for Day6<'a> {
    fn day(&self) -> u8 {
        6
    }

    fn cache(&self) -> &Input {
        self.cache
    }

    fn run(&self) -> Result<String, Box<dyn std::error::Error>> {
        self.do_run()
    }
}

pub fn new<'a>(cache: &'a Input) -> impl Day + 'a {
    Day6 { cache }
}
