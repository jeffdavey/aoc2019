use config::{Config, File};
use input::Input;
use snafu::{ResultExt, Snafu};
use snafu_cli_debug::SnafuCliDebug;
use std::path::Path;

mod day1;
mod day10;
mod day2;
mod day3;
mod day4;
mod day5;
mod day6;
mod day7;
mod day8;
mod day9;
mod intcode;

#[derive(Snafu, SnafuCliDebug)]
pub enum Error {
    Configuration { source: config::ConfigError },
    InputCache { source: input::Error },
    DoRun { source: Box<dyn std::error::Error> },
    ParseDay { source: std::num::ParseIntError },
}

pub trait Day {
    fn day(&self) -> u8;
    fn cache(&self) -> &Input;
    fn input(&self) -> Result<String, input::Error> {
        self.cache().get(self.day())
    }
    fn run(&self) -> Result<String, Box<dyn std::error::Error>>;
}

fn main() -> Result<(), Error> {
    let args: Vec<String> = std::env::args().collect();
    let mut settings = Config::default();
    settings.merge(File::with_name("config.toml")).context(Configuration)?;
    let cache_path = settings.get_str("cache_path").context(Configuration)?;
    let url = settings.get_str("url").context(Configuration)?;
    let session = settings.get_str("session").context(Configuration)?;

    let input_cache = Input::open(&url, &session, &Path::new(&cache_path)).context(InputCache)?;

    let days: Vec<Box<dyn Day>> = vec![
        Box::new(day1::new(&input_cache)),
        Box::new(day2::new(&input_cache)),
        Box::new(day3::new(&input_cache)),
        Box::new(day4::new(&input_cache)),
        Box::new(day5::new(&input_cache)),
        Box::new(day6::new(&input_cache)),
        Box::new(day7::new(&input_cache)),
        Box::new(day8::new(&input_cache)),
        Box::new(day9::new(&input_cache)),
        Box::new(day10::new(&input_cache)),
    ];

    let specific_day = if args.len() > 1 {
        Some(args[1].parse::<i32>().context(ParseDay)?)
    } else {
        None
    };
    days.iter()
        .filter_map(|day| {
            if let Some(specific_day) = specific_day {
                if day.day() == specific_day as u8 {
                    Some(match day.run() {
                        Ok(r) => {
                            println!("Day {} - {}", day.day().to_string(), r);
                            Ok(r)
                        }
                        Err(e) => Err(e),
                    })
                } else {
                    None
                }
            } else {
                Some(match day.run() {
                    Ok(r) => {
                        println!("Day {} - {}", day.day().to_string(), r);
                        Ok(r)
                    }
                    Err(e) => Err(e),
                })
            }
        })
        .collect::<Result<Vec<_>, _>>()
        .context(DoRun)?;
    Ok(())
}
