use crate::Day;
use fraction::Decimal;
use input::Input;
use snafu::{ResultExt, Snafu};
use std::collections::HashSet;

#[derive(Debug, Snafu)]
pub enum Error {
    RetrievingInput { source: input::Error },
    ParsingInput { source: std::num::ParseIntError },
    Lines,
}

pub struct Day10<'a> {
    cache: &'a Input,
}

#[derive(Debug)]
struct Asteroid {
    point: (usize, usize),
    slope: f64,
    distance: f64,
    vaporized: bool,
}

impl<'a> Day for Day10<'a> {
    fn day(&self) -> u8 {
        10
    }

    fn cache(&self) -> &Input {
        self.cache
    }

    fn run(&self) -> Result<String, Box<dyn std::error::Error>> {
        let input = self
            .input()
            .context(RetrievingInput)?
            .lines()
            .map(|s| s.chars().collect::<Vec<_>>())
            .collect::<Vec<_>>();

        let height = input.len();
        let width = input[0].len();

        let mut part1 = (0, 0, 0);
        for (y, line) in input.iter().enumerate() {
            for (x, tile) in line.iter().enumerate() {
                if *tile == '#' {
                    // asteroid
                    let mut hits = HashSet::new();
                    for target_y in 0..height {
                        for target_x in 0..width {
                            if input[target_y][target_x] == '#' {
                                if !(target_x == x && target_y == y) {
                                    let slope = ((target_x as isize - x as isize) as f64).atan2((target_y as isize - y as isize) as f64);
                                    hits.insert(Decimal::from(slope));
                                }
                            }
                        }
                    }
                    if hits.len() > part1.2 {
                        part1 = (x, y, hits.len())
                    }
                }
            }
        }

        let mut asteroids = Vec::new();
        let x = part1.0;
        let y = part1.1;
        for target_y in 0..height {
            for target_x in 0..width {
                if input[target_y][target_x] == '#' {
                    if !(target_x == x && target_y == y) {
                        let slope = ((target_x as isize - x as isize) as f64).atan2((target_y as isize - y as isize) as f64);
                        let distance = (((target_x as isize - x as isize) as f64).powi(2) + ((target_y as isize - y as isize) as f64).powi(2)).sqrt();
                        asteroids.push(Asteroid {
                            point: (target_x, target_y),
                            slope,
                            distance,
                            vaporized: false,
                        })
                    }
                }
            }
        }

        asteroids.sort_by(|a, b| {
            let a_slope = Decimal::from(a.slope);
            let b_slope = Decimal::from(b.slope);
            let a_distance = Decimal::from(a.distance);
            let b_distance = Decimal::from(b.distance);

            if a_slope == b_slope {
                return b_distance.partial_cmp(&a_distance).unwrap();
            }
            b_slope.partial_cmp(&a_slope).unwrap()
        });
        let mut destroyed = 0;
        let mut part2 = 0;
        loop {
            let mut last_roid = 0f64;
            for roid in &mut asteroids {
                if !roid.vaporized && roid.slope != last_roid {
                    destroyed += 1;
                    if destroyed == 200 {
                        part2 = roid.point.0 * 100 + roid.point.1;
                    }
                    roid.vaporized = true;
                    last_roid = roid.slope;
                }
            }
            if last_roid == 0f64 {
                break;
            }
        }

        Ok(format!("Part1: {} Part2 {}", part1.2, part2))
    }
}

pub fn new<'a>(cache: &'a Input) -> impl Day + 'a {
    Day10 { cache }
}
