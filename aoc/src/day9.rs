use crate::intcode::{Computer, IntCodeIO};
use crate::Day;
use input::Input;
use snafu::{ResultExt, Snafu};

#[derive(Debug, Snafu)]
pub enum Error {
    RetrievingInput { source: input::Error },
    ParsingInput { source: std::num::ParseIntError },
}

pub struct Day9<'a> {
    cache: &'a Input,
}

impl<'a> Day for Day9<'a> {
    fn day(&self) -> u8 {
        9
    }

    fn cache(&self) -> &Input {
        self.cache
    }

    fn run(&self) -> Result<String, Box<dyn std::error::Error>> {
        let opcodes = self
            .input()
            .context(RetrievingInput)?
            .trim()
            .split(',')
            .map(|s| s.parse::<i64>())
            .collect::<Result<Vec<_>, _>>()
            .context(ParsingInput)?;

        let part1 = IntCodeIO::new();
        let input = IntCodeIO::from_values(vec![1]);
        let mut computer = Computer::new(&opcodes);
        computer.run(Some(input), Some(part1.clone()))?;
        computer.reset();
        let part2 = IntCodeIO::new();
        let input = IntCodeIO::from_values(vec![2]);
        computer.run(Some(input), Some(part2.clone()))?;
        Ok(format!("Part 1: {}  Part 2: {}", part1.values()[0], part2.values()[0]))
    }
}

pub fn new<'a>(cache: &'a Input) -> impl Day + 'a {
    Day9 { cache }
}
