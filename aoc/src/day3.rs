use crate::Day;
use input::Input;
use snafu::{ResultExt, Snafu};
use std::collections::HashMap;

#[derive(Debug, Snafu)]
pub enum Error {
    RetrievingInput {
        source: input::Error,
    },
    ParseInput {
        source: std::num::ParseIntError,
    },
    #[snafu(display("Unknown movement direction '{}'", char))]
    Unknown {
        char: String,
    },
}

pub struct Day3<'a> {
    cache: &'a Input,
}

#[derive(Debug)]
pub enum Movement {
    Up(i32),
    Down(i32),
    Right(i32),
    Left(i32),
}

impl Movement {
    pub fn parse(s: &str) -> Result<Movement, Error> {
        match (&s[0..1], &s[1..]) {
            ("R", amount) => Ok(Movement::Right(amount.parse().context(ParseInput)?)),
            ("L", amount) => Ok(Movement::Left(amount.parse().context(ParseInput)?)),
            ("U", amount) => Ok(Movement::Up(amount.parse().context(ParseInput)?)),
            ("D", amount) => Ok(Movement::Down(amount.parse().context(ParseInput)?)),
            (val, _) => Err(Error::Unknown { char: val.to_string() }),
        }
    }
}

impl<'a> Day3<'a> {
    fn parse_movements(&self, movements: &Vec<Movement>) -> HashMap<(i32, i32), i32> {
        let mut wire = HashMap::new();
        let mut pos = (0, 0);
        let mut steps = 1;
        for movement in movements {
            match movement {
                Movement::Right(distance) => {
                    for _ in 0..*distance {
                        pos = (pos.0 + 1, pos.1);
                        wire.insert(pos, steps);
                        steps += 1;
                    }
                }
                Movement::Left(distance) => {
                    for _ in 0..*distance {
                        pos = (pos.0 - 1, pos.1);
                        wire.insert(pos, steps);
                        steps += 1;
                    }
                }
                Movement::Up(distance) => {
                    for _ in 0..*distance {
                        pos = (pos.0, pos.1 + 1);
                        wire.insert(pos, steps);
                        steps += 1;
                    }
                }
                Movement::Down(distance) => {
                    for _ in 0..*distance {
                        pos = (pos.0, pos.1 - 1);
                        wire.insert(pos, steps);
                        steps += 1;
                    }
                }
            }
        }
        wire
    }
    fn do_run(&self) -> Result<String, Box<dyn std::error::Error>> {
        let movements = self
            .input()
            .context(RetrievingInput)?
            .lines()
            .map(|l| l.trim().split(',').map(|s| Movement::parse(s)).collect::<Result<Vec<_>, _>>())
            .collect::<Result<Vec<_>, _>>()?;

        let wire1 = self.parse_movements(&movements[0]);
        let wire2 = self.parse_movements(&movements[1]);

        let mut part1 = 0;
        let mut part2 = 0;
        for (pos, steps1) in &wire1 {
            if let Some(steps2) = wire2.get(pos) {
                let distance = i32::abs(pos.0) + i32::abs(pos.1);
                let steps = steps1 + steps2;
                if part1 == 0 || distance < part1 {
                    part1 = distance;
                }
                if part2 == 0 || steps < part2 {
                    part2 = steps;
                }
            }
        }
        Ok(format!("Part1 {} Part2 {}", part1, part2))
    }
}

impl<'a> Day for Day3<'a> {
    fn day(&self) -> u8 {
        3
    }

    fn cache(&self) -> &Input {
        self.cache
    }

    fn run(&self) -> Result<String, Box<dyn std::error::Error>> {
        self.do_run()
    }
}

pub fn new<'a>(cache: &'a Input) -> impl Day + 'a {
    Day3 { cache }
}
