use crate::intcode::{self, Computer, IntCodeIO};
use crate::Day;
use input::Input;
use snafu::{ResultExt, Snafu};
use std::thread;

#[derive(Debug, Snafu)]
pub enum Error {
    RetrievingInput { source: input::Error },
    ParseInput { source: std::num::ParseIntError },
    IntCode { source: intcode::Error },
    JoinFailure,
}

pub struct Day7<'a> {
    cache: &'a Input,
}

impl<'a> Day7<'a> {
    fn do_run(&self) -> Result<String, Box<dyn std::error::Error>> {
        let opcodes = self
            .input()
            .context(RetrievingInput)?
            .trim()
            .split(',')
            .map(|s| s.parse::<i64>())
            .collect::<Result<Vec<_>, _>>()
            .context(ParseInput)?;

        let mut part1 = 0;
        let permutated_phases = permute::permute(vec![0i64, 1, 2, 3, 4]);
        let mut amps = (0..5).map(|_| Computer::new(&opcodes)).collect::<Vec<_>>();

        for phases in permutated_phases {
            let mut last_output = 0;
            for (phase, amp) in phases.iter().zip(&mut amps) {
                amp.reset();
                let input = IntCodeIO::from_values(vec![*phase, last_output]);
                let output = IntCodeIO::new();
                amp.run(Some(input), Some(output.clone()))?;
                last_output = output.values()[0];
            }
            if last_output > part1 {
                part1 = last_output;
            }
        }

        let permutated_phases = permute::permute(vec![5i64, 6, 7, 8, 9]);
        let mut part2 = 0;
        for phases in permutated_phases {
            let mut inputs = Vec::new();
            for phase in &phases {
                inputs.push(IntCodeIO::from_values(vec![*phase]));
            }
            inputs[0].push(0);

            let mut index = 0;
            let mut threads = Vec::new();
            for _ in &phases {
                threads.push(thread::spawn({
                    let input = inputs[index].clone();
                    let output = if index == phases.len() - 1 {
                        inputs[0].clone()
                    } else {
                        inputs[index + 1].clone()
                    };
                    let opcodes = opcodes.to_vec();
                    move || {
                        let mut amp = Computer::new(&opcodes);
                        amp.run(Some(input), Some(output))
                    }
                }));
                index += 1;
            }

            while let Some(thread) = threads.pop() {
                match thread.join() {
                    Ok(val) => val?,
                    Err(_e) => Err(Error::JoinFailure)?,
                }
            }
            let current = inputs[0].values()[0];
            if current > part2 {
                part2 = current;
            }
        }
        Ok(format!("Part 1: {} Part 2: {:?}", part1, part2))
    }
}

impl<'a> Day for Day7<'a> {
    fn day(&self) -> u8 {
        7
    }

    fn cache(&self) -> &Input {
        self.cache
    }

    fn run(&self) -> Result<String, Box<dyn std::error::Error>> {
        self.do_run()
    }
}

pub fn new<'a>(cache: &'a Input) -> impl Day + 'a {
    Day7 { cache }
}
